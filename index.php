<?php require_once("includes/session.php"); ?>
<?php require_once("includes/connection.php"); ?>
<?php require_once("includes/functions.php"); 
?>
<?php

echo "<center><h1>SITE UNDER MAINTENANCE</h1></center>";

exit();

//registration procees
if(isset($_POST['register']))
{

    $designation = mysqli_real_escape_string($mysqli,$_POST['designation']);
    $fname = mysqli_real_escape_string($mysqli,$_POST['fname']);
    $lname = mysqli_real_escape_string($mysqli,$_POST['lname']);
    $email = mysqli_real_escape_string($mysqli,$_POST['email']);
    $zone = mysqli_real_escape_string($mysqli,$_POST['zone']);
    $kcnumber = mysqli_real_escape_string($mysqli,$_POST['kcnumber']);
    $time = time();


    $error = '';

    //validate full name field
    if($designation==''){
        $error = 'Please enter your Designation';
    }

        //validate full name field
        if($fname==''){
          $error = 'Please enter your first name';
      }
        //validate full name field
        if($lname==''){
          $error = 'Please enter your last name';
      }

            //validate full name field
            if($email==''){
              $error = 'Please enter your email address';
          }

              //validate full name field
              if($zone==''){
                $error = 'Please enter your zone';
            }
  
    


  //validate mobile field
  else if($kcnumber == ''){
      $error = 'Please enter your kingschat mobile number';
  }
  

    else if(!is_numeric($kcnumber)){
        $error = 'Invalid mobile number, please enter a correct phone number';
    }

   
  $chkemail1 = $mysqli->prepare("SELECT kcnumber FROM register WHERE kcnumber= ? ");
  $chkemail1->bind_param("s", $kcnumber);
  $chkemail1->execute();
  $email_fetch1 = $chkemail1->get_result();
  $email_part1 = $email_fetch1->fetch_assoc();
  
  if($email_fetch1->num_rows === 1){
      
      
   
        $error = 'An account with this phone number already exist';
      
  }

 

    //insert data into tabel
    else{


$reg_user = $mysqli->prepare("insert into register(title,fname,lname,email,zone,kcnumber,time) values(?,?,?,?,?,?,?)");
$reg_user->bind_param("sssssss", $designation,$fname,$lname,$email,$zone,$kcnumber,$time);
$reg_user->execute();
if($reg_user->affected_rows === 1)
 {
             
  echo "
  <script>
  alert('Thank you for registering. See you at the conference.');
  </script>
  ";
   // redirect_to(SITE_PATH.'login.php');
            }
        else{
            $error = "Registration failed, please try again.";
            echo "
            <script>
            alert('We have an issue with your registration');
            </script>
            ";
            printf("Error: %s.\n", $reg_user->error);
            $reg_user->close();
            }
            
            
      
    
    }
if($error=="success"){

}else{
  
}


}




?>
<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <title>RPC Online</title>
    
    <!-- Loading Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Template CSS -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css">
    <link href="css/style-magnific-popup.css" rel="stylesheet">

    <!-- Awsome Fonts -->
    <link rel="stylesheet" href="css/all.min.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,400i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:500,700&display=swap" rel="stylesheet">

    <!-- Font Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">
    
</head>

<body>

    <!--begin header -->
    <header class="header">

        <!--begin navbar-fixed-top -->
        <nav class="navbar navbar-default navbar-fixed-top">
            
            <!--begin container -->
            <div class="container">

                <!--begin navbar -->
                <nav class="navbar navbar-expand-lg">

                    <!--begin logo -->
                    <a class="navbar-brand" href="#"><img src="logo.png" style="width: 70px;"/></a>
                    <!--end logo -->

                    <!--begin navbar-toggler -->
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                    </button>
                    <!--end navbar-toggler -->

                    <!--begin navbar-collapse -->
                    <div class="navbar-collapse collapse" id="navbarCollapse" style="">
                        
                        <!--begin navbar-nav -->
                        <ul class="navbar-nav ml-auto">


                        </ul>
                        <!--end navbar-nav -->

                    </div>
                    <!--end navbar-collapse -->

                </nav>
                <!--end navbar -->

            </div>
    		<!--end container -->
            
        </nav>
    	<!--end navbar-fixed-top -->
        
    </header>
    <!--end header -->

    <!--begin home section -->
    <section class="home-section" id="home">

        <div class="home-section-overlay"></div>
           <video autoplay playsinline muted loop>
  <source src="rpc.mp4" type="video/mp4">
</video>

		<!--begin container -->
		<div class="container">

	        <!--begin row --d-md-block>
	        <div class="row">
	          <div class="col-md-7  text-center wow bounceIn d-none ">

            <br/>  <p style="text-align:justify; color:beige; padding:20px;">As the International Pastors and Partners Conference wraps up, we kick off the new ministry year with The Rhapsody Partnership Conference
(RPC) 2021.  <br/>The conference is a two-day conference that aims to bring together all-volunteer networks, Heads of Departments, Pastors,
translators and zonal managers to communicate the objectives and targets of the Rhapsody of Realities department for 2022. 
<br/><br/><b>Proposed Date:</b> Monday, 22nd - Tuesday, 23rd November 2021<br/>
<b>Proposed Time:</b> 9am (GMT + 1)<br/>
<b>Proposed Venue:</b> The Angel Court, Asese Campground</p>
              </div>
                <!--begin col-md-5-->
                <div class="col-md-5  offset-md-7  text-center wow bounceIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceIn;">

                    <!--begin register-form-wrapper-->
                    <div class="register-form-wrapper wow bounceIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceIn;">

                        <h1>RHAPSODY OF REALITIES PARTNERSHIP CONFERENCE</h1>
                  
                   
                        <!--begin form-->
                        <div>
                             
                     
                            
                            <!--begin register form -->
                            <form style="text-align: left;" class="register-form register" action="" method="post">
                                <label>Title:</label>
                            <select name="designation" class="register-input name-input white-input">
                                <option value="">Select Option</option>
                                <option value="Reverend">Reverend</option>
                                <option value="Evangelist">Evangelist</option>
                                <option value="Pastor">Pastor</option>
                                <option value="Deacon">Deacon</option>
                                <option value="Deaconess">Deaconess</option>
                                <option value="Brother" selected>Brother</option>
                                <option value="Sister">Sister</option>
                        </select>
                        <div class="row">
                            <div class="col-md-6">
                            <label>First Name:</label>

                                <input class="register-input white-input" required="" name="fname" placeholder="First Name" type="text">
</div>
<div class="col-md-6">
<label>Last Name:</label>

<input class="register-input white-input" required="" name="lname" placeholder="Last Name" type="text">
                         </div>
                        </div>
                        <label>Email Address:</label>

<input class="register-input name-email white-input" required="" name="email" placeholder="Email Adress*" type="email">
<label>Zone:</label>

<select name="zone" class="register-input white-input">
    <option value="">Select Option</option><option value="Aba Zone">Aba Zone</option>
    <option value="Abeokuta Ministry Center">Abeokuta Ministry Center</option>
    <option value="Abuja Ministry Center">Abuja Ministry Center</option>
    <option value="Abuja Zone">Abuja Zone</option>
    <option value="Accra Zone">Accra Zone</option>
    <option value="Benin Zone 1" selected>Benin Zone 1</option>
    <option value="Benin Zone 2">Benin Zone 2</option>
    <option value="Calabar Ministry Center">Calabar Ministry Center</option>
    <option value="Canada Zone">Canada Zone</option>
    <option value="CE AUSTRALIA">CE AUSTRALIA</option>
    <option value="CE CHAD">CE CHAD</option>
    <option value="CE EASTERN EUROPE">CE EASTERN EUROPE</option>
    <option value="CE Texas Zone 1">CE Texas Zone 1</option>
    <option value="CE Texas Zone 2 (USA REGION 3)">CE Texas Zone 2 (USA REGION 3)</option>
    <option value="Edo North &amp; Central Zone">Edo North &amp; Central Zone</option>
    <option value="EWC Zone 1">EWC Zone 1</option>
    <option value="EWC Zone 2">EWC Zone 2</option>
    <option value="EWC Zone 3">EWC Zone 3</option>
    <option value="EWC Zone 4">EWC Zone 4</option>
    <option value="EWC Zone 5">EWC Zone 5</option>
    <option value="EWC Zone 6">EWC Zone 6</option>
    <option value="Kenya Zone">Kenya Zone</option>
    <option value="Lagos Virtual Zone">Lagos Virtual Zone</option>
    <option value="Lagos Zone 1">Lagos Zone 1</option>
    <option value="Lagos Zone 2">Lagos Zone 2</option>
    <option value="Lagos Zone 3">Lagos Zone 3</option>
    <option value="Lagos Zone 4">Lagos Zone 4</option>
    <option value="Lagos Zone 5">Lagos Zone 5</option>
    <option value="BLW 2ND TIER CHURCH">BLW 2ND TIER CHURCH</option>
    <option value="BLW CAMEROUN GROUP 1">BLW CAMEROUN GROUP 1</option>
    <option value="BLW CAMEROUN GROUP 2">BLW CAMEROUN GROUP 2</option>
    <option value="BLW GHANA ZONE A">BLW GHANA ZONE A</option>
    <option value="BLW GHANA ZONE B">BLW GHANA ZONE B</option>
    <option value="BLW KENYA ZONE">BLW KENYA ZONE</option>
    <option value="BLW SA ZONE A">BLW SA ZONE A</option>
    <option value="BLW SA ZONE B">BLW SA ZONE B</option>
    <option value="BLW SA ZONE C">BLW SA ZONE C</option>
    <option value="BLW SA ZONE D">BLW SA ZONE D</option>
    <option value="BLW SA ZONE E">BLW SA ZONE E</option>
    <option value="BLW UGANDA">BLW UGANDA</option>
    <option value="BLW UK ZONE A">BLW UK ZONE A</option>
    <option value="BLW UK ZONE B">BLW UK ZONE B</option>
    <option value="BLW USA GROUP 1">BLW USA GROUP 1</option>
    <option value="BLW USA GROUP 2">BLW USA GROUP 2</option>
    <option value="BLW USA GROUP 3">BLW USA GROUP 3</option>
    <option value="BLW ZONE A">BLW ZONE A</option>
    <option value="BLW ZONE B">BLW ZONE B</option>
    <option value="BLW ZONE C">BLW ZONE C</option>
    <option value="BLW ZONE D">BLW ZONE D</option>
    <option value="BLW ZONE E">BLW ZONE E</option>
    <option value="BLW ZONE F">BLW ZONE F</option>
    <option value="BLW ZONE G">BLW ZONE G</option>
    <option value="BLW ZONE H">BLW ZONE H</option>
    <option value="BLW ZONE I">BLW ZONE I</option>
    <option value="BLW ZONE J">BLW ZONE J</option>
    <option value="BLW ZONE L">BLW ZONE L</option>
    <option value="BLW ZONE L">BLW ZONE L</option>
    <option value="Middle East Zone">Middle East Zone</option>
    <option value="Midwest Zone">Midwest Zone</option>
    <option value="NC  Zone 2">NC  Zone 2</option>
    <option value="NC Zone 1">NC Zone 1</option>
    <option value="NE Zone 1">NE Zone 1</option>
    <option value="NW Zone 1">NW Zone 1</option>
    <option value="NW Zone 2">NW Zone 2</option>
    <option value="Onitsha Zone">Onitsha Zone</option>
    <option value="Port Harcourt Ministry Center">Port Harcourt Ministry Center</option>
    <option value="Port Harcourt Zone 1">Port Harcourt Zone 1</option>
    <option value="Port Harcourt Zone 2">Port Harcourt Zone 2</option>
    <option value="Port Harcourt Zone 3">Port Harcourt Zone 3</option>
    <option value="Quebec Zone">Quebec Zone</option>
    <option value="SA Zone 1">SA Zone 1</option>
    <option value="SA Zone 2">SA Zone 2</option>
    <option value="SA Zone 3">SA Zone 3</option>
    <option value="SA Zone 4">SA Zone 4</option>
    <option value="SA Zone 5">SA Zone 5</option>
    <option value="SE Zone 1">SE Zone 1</option>
    <option value="SE Zone 2">SE Zone 2</option>
    <option value="South America Region">South America Region</option>
    <option value="SS Zone 1">SS Zone 1</option>
    <option value="SS Zone 2">SS Zone 2</option>
    <option value="SW Zone 1">SW Zone 1</option>
    <option value="SW Zone 2">SW Zone 2</option>
    <option value="SW Zone 3">SW Zone 3</option>
    <option value="UK Zone 1 (Region 1)">UK Zone 1 (Region 1)</option>
    <option value="UK Zone 1 (UK Region 2)">UK Zone 1 (UK Region 2)</option>
    <option value="UK Zone 2 (Region 1)">UK Zone 2 (Region 1)</option>
    <option value="UK Zone 3 (Region 1)">UK Zone 3 (Region 1)</option>
    <option value="UK Zone 3 (UK Region 2)">UK Zone 3 (UK Region 2)</option>
    <option value="UK Zone 4 (Region 1)">UK Zone 4 (Region 1)</option>
    <option value="UK Zone 4(UK Region 2)">UK Zone 4(UK Region 2)</option>
    <option value="USA Zone 1 (Region 1)">USA Zone 1 (Region 1)</option>
    <option value="USA Zone 1 (Region 2)">USA Zone 1 (Region 2)</option>
    <option value="USA Zone 2 (Region 1)">USA Zone 2 (Region 1)</option>
    <option value="Warri Ministry Center">Warri Ministry Center</option>
    <option value="Western Europe Zone 1">Western Europe Zone 1</option>
    <option value="Western Europe Zone 2">Western Europe Zone 2</option>
    <option value="Western Europe Zone 3">Western Europe Zone 3</option>
    <option value="Western Europe Zone 4">Western Europe Zone 4</option>
    <option value="TNI">TNI</option><option value="REON">REON</option>
    <option value="REON Youth">REON Youth</option>
    <option value="ISM">ISM</option><option value="GYLF">GYLF</option>
    <option value="Other">Other...</option>
</select>

<label>Kingschat Number:</label>

<input class="register-input white-input" required="" name="kcnumber" placeholder="Kingschat Number" type="text">


                                <input value="Register" name="register" class="register-submit" type="submit">
                                    
                            </form>
                            <!--end register form -->

                            
                        </div>
                        <!--end form-->

                    </div>
                    <!--end register-form-wrapper-->

                </div>
                <!--end col-md-5-->

	        </div>
	        <!--end row -->

		</div>
		<!--end container -->

    </section>
    <!--end home section -->

    

 


<!-- Load JS here for greater good =============================-->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.scrollTo-min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/wow.js"></script>
<script src="js/countdown.js"></script>
<script src="js/plugins.js"></script>
<script src="js/custom.js"></script>


</body>
</html>