<?php
foreach (array_merge($_GET, $_POST) as $key => $val) {
  global $$key;
  $$key = addslashes($val);
}

require("constants.php");

// 1. Create a database connection Procedural style
$connection = mysqli_connect(DB_SERVER,DB_USER,DB_PASS);
if (!$connection) {
	die("Database connection failed: " . mysqli_error($connection));
}

// 2. Select a database to use 
$db_select = mysqli_select_db($connection, DB_NAME);
if (!$db_select) {
	die("Database selection failed: " . mysqli_error($connection));
}



$db_username = DB_USER;
$db_password = DB_PASS;
$db_name = DB_NAME;
$db_host = 'localhost';

$mysqli = new mysqli($db_host, $db_username, $db_password, $db_name);
//Output any connection error
if ($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}

?>
