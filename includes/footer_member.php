
    
    <div class="footer-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <span class="footer-copyright">Copyright 2018, <a href="#"><?php echo SITE_NAME; ?></a>.  All Rights Reserved.</span>
                </div><!-- .col -->
                <div class="col-md-5 text-md-right">
                    <ul class="footer-links">
                        <li><a href="<?php echo SITE_PATH; ?>privacy">Privacy Policy</a></li>
                        <li><a href="<?php echo SITE_PATH; ?>terms">Terms of use</a></li>
                    </ul>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div>
    <!-- FooterBar End -->
    
 	<!-- JavaScript (include all script here) -->
	 <script src="<?php echo SITE_PATH; ?>assets/js/jquery.bundleba3a.js?ver=101"></script>
	<script src="<?php echo SITE_PATH; ?>assets/js/scriptba3a.js?ver=101"></script>
</body>

</html>

