 <div class="sidebar" data-active-color="green" data-background-color="black" data-image="<?php echo SITE_PATH; ?>assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
        Tip 2: you can also add an image using data-image tag
        Tip 3: you can change the color of the sidebar with data-background-color="white | black"
    -->
            <div class="logo">
                <a href="#" class="simple-text logo-mini">
                   FT
                </a>
                <a href="#" class="simple-text logo-normal">
                   First Timer<br/>Management
                </a>
            </div>
            <div class="sidebar-wrapper">
                <div class="user">
                  
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            <span>
                              <?php echo adminbyid($_SESSION['superadmid'],'UserName') ?> 
                                <b class="caret"></b>
                            </span>
                        </a>
                        <div class="clearfix"></div>
                        <div class="collapse" id="collapseExample">
                            <ul class="nav">
                              
                             <li>
                                      <a href="<?php echo SITE_PATH; ?>superadmin/changepass.php">
                                        <span class="sidebar-mini"> EP </span>
                                        <span class="sidebar-normal"> Change Password </span>
                                    </a>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="nav">
                    <li>
                        <a href="<?php echo SITE_PATH; ?>superadmin/account.php">
                            <i class="material-icons">dashboard</i>
                            <p> Dashboard </p>
                        </a>
                    </li>

                 
                      <li>
                        <a href="<?php echo SITE_PATH; ?>superadmin/officers.php">
                            <i class="material-icons">person</i>
                            <p> Members  (<?php  echo countuser_all_1(); ?>)
</p>
                        </a>
                    </li>
                    
                    <li>
                        <a href="<?php echo SITE_PATH; ?>superadmin/newft.php">
                            <i class="material-icons">date_range</i>
                            <p> New FT </p>
                        </a>
                    </li>


                       <li>
                        <a data-toggle="collapse" href="#formsExamples11">
                            <i class="material-icons">timeline</i>
                            <p>Manage Groups
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="formsExamples11">
                            <ul class="nav">
                               
                                <li>
                                    <a href="<?php echo SITE_PATH; ?>superadmin/newgrp.php">
                                        <span class="sidebar-mini"> MF </span>
                                        <span class="sidebar-normal"> New Group  </span>
                                    </a>
                                </li>
                             
                                <li>
                                    <a href="<?php echo SITE_PATH; ?>superadmin/managegrp.php">
                                        <span class="sidebar-mini"> MF </span>
                                        <span class="sidebar-normal"> Manage FT  </span>
                                    </a>
                                </li>
                                
                            
                                
                                
                            </ul>
                        </div>
                    </li>
                   

                     
                    <li>
                        <a data-toggle="collapse" href="#formsExamples">
                            <i class="material-icons">timeline</i>
                            <p>Manage First TImers
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="formsExamples">
                            <ul class="nav">
                               
                             
                                <li>
                                    <a href="<?php echo SITE_PATH; ?>superadmin/manageft.php">
                                        <span class="sidebar-mini"> MF </span>
                                        <span class="sidebar-normal"> Manage FT  </span>
                                    </a>
                                </li>
                                
                            
                                
                                
                            </ul>
                        </div>
                    </li>
                   
                  
                    <li>
                        <a href="<?php echo SITE_PATH; ?>superadmin/logout.php">
                            <i class="material-icons">date_range</i>
                            <p> Logout </p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>