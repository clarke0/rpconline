
                <div class="side_foot_links">
                    <a href="#">Contact Us </a>
                    <a href="#">Terms of service</a>
                </div>

            </div>

            <div class="side_overly" uk-toggle="target: #wrapper ; cls: is-collapse is-active"></div>
        </div>
        
    </div>

 
    <!-- Javascript
    ================================================== -->
    <script src="jquery-3.6.0.min.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/js/uikit.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/js/tippy.all.min.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/js/simplebar.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/js/custom.js"></script>
    <script src="<?php echo SITE_PATH; ?>assets/js/bootstrap-select.min.js"></script>
    <script src="http://unpkg.com/ionicons%405.2.3/dist/ionicons.js"></script>

</body>
</html>
