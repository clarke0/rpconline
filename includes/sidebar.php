<div class="sidebar">
            <div class="sidebar_inner b" data-simplebar>
                
                <ul class="side-colored">
                    <li class="active"><a href="<?php echo SITE_PATH; ?>">
                            <ion-icon name="compass" class="bg-gradient-to-br from-purple-300 p-1 rounded-md side-icon text-opacity-80 text-white to-blue-500">
                            </ion-icon>
                            <span> Home</span>
                        </a>
                    </li>
                    <li><a href="<?php echo SITE_PATH; ?>member.php">
                            <ion-icon name="play-circle" class="bg-gradient-to-br from-yellow-300 p-1 rounded-md side-icon text-opacity-80 text-white to-red-500">
                            </ion-icon>
                            <span> Live Service</span>
                        </a>
                    </li>
                    <?php if(!logged_in()){
              
              ?>
                    <li><a href="<?php echo SITE_PATH; ?>login.php">
                            <ion-icon name="albums"  class="bg-gradient-to-br from-green-300 p-1 rounded-md side-icon text-opacity-80 text-white to-green-500">
                            </ion-icon>
                            <span> Login </span>
                        </a>
                    </li>
                
                    <li><a href="<?php echo SITE_PATH; ?>signup.php">
                            <ion-icon name="film" class="bg-gradient-to-br from-pink-300 p-1 rounded-md side-icon text-opacity-80 text-white to-red-500">
                            </ion-icon>
                            <span> Signup </span>
                        </a>
                    </li>

                    <?php
            }else{  ?>

<li><a href="<?php echo SITE_PATH; ?>logout.php">
                            <ion-icon name="albums"  class="bg-gradient-to-br from-green-300 p-1 rounded-md side-icon text-opacity-80 text-white to-green-500">
                            </ion-icon>
                            <span> Logout </span>
                        </a>
                    </li>   
<?php
            }

            ?>
               

                    <li><a href="<?php echo SITE_PATH; ?>#">
                            <ion-icon name="film" class="bg-gradient-to-br from-pink-300 p-1 rounded-md side-icon text-opacity-80 text-white to-red-500">
                            </ion-icon>
                            <span> News & Updates </span>
                        </a>
                    </li>
          
                </ul>
