<?php
//ini_set('session.save_path',realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/../nhsses'));
	session_start();
	
	function admin_login() {
		return isset($_SESSION['admid']);
		return isset($_SESSION['admuser']);
	}
	
	
	function admin_logged_in() {
		if (!admin_login()) {
			redirect_to(SITE_PATH."groupadmin/index.php");
			exit();
		}
	}
	

	function superadmin_login() {
		return isset($_SESSION['superadmid']);
		return isset($_SESSION['superadmuser']);
	}
	
	
	function superadmin_logged_in() {
		if (!superadmin_login()) {
			redirect_to(SITE_PATH."superadmin/index.php");
			exit();
		}
	}
	


	
	
	function logged_in() {
		return isset($_SESSION['id']);
		return isset($_SESSION['phone']);
		return isset($_SESSION['status']);
	}
	
	function confirm_logged_in() {
		if (!logged_in()) {
			redirect_to(SITE_PATH."login.php");
			exit();
		}
	}
?>
