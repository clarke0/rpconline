<!DOCTYPE html>
<html lang="en">
<head> 

    <!-- Basic Page Needs
    ================================================== -->
    <title><?php echo $pagetitle ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Welcome to Christ Embassy Giwaamu, Benin Zone 1">

    <!-- Favicon -->
    <link href="<?php echo SITE_PATH; ?>assets/images/favicon.png" rel="icon" type="image/png">

    <!-- icons
    ================================================== -->
    <link rel="stylesheet" href="<?php echo SITE_PATH; ?>assets/css/icons.css">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo SITE_PATH; ?>assets/css/uikit.css">
    <link rel="stylesheet" href="<?php echo SITE_PATH; ?>assets/scss/style.css">
    <link rel="stylesheet" href="<?php echo SITE_PATH; ?>assets/css/tailwind.css">

</head>

<body>

    <div id="wrapper" class="is-verticle">

        <!--  Header   uk-sticky="cls-inactive: border-b"-->
        <header class="is-transparent is-dark border-b" uk-sticky="cls-inactive: border-b"> 
            <div class="header_inner">
                <div class="left-side">
    
                    <!-- Logo -->
                    <div id="logo">
                        <a href="explore.html">
                            <img src="<?php echo SITE_PATH; ?>images/<?php echo settingbyid('logo_img'); ?>" alt="<?php echo settingbyid('site_name'); ?>" alt="">
                            <img src="<?php echo SITE_PATH; ?>images/<?php echo settingbyid('logo_img'); ?>" alt="<?php echo settingbyid('site_name'); ?>" class="logo_inverse" alt="">
                            <img src="<?php echo SITE_PATH; ?>images/<?php echo settingbyid('logo_img'); ?>" alt="<?php echo settingbyid('site_name'); ?>" class="logo_mobile" alt="">
                        </a>
                    </div>
    
                    <!-- icon menu for mobile -->
                    <div class="triger" uk-toggle="target: #wrapper ; cls: is-active">
                    </div>
    
                </div>
               
            </div>
        </header>

        <!-- Main Contents -->
        <div class="main_content">

          